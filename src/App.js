import React from 'react';
import Header from './components/Header/Header';
import DayCalender from './components/DayCalender/DayCalender';
import './App.css';

function App() {
  return (
    <div className="App">
    <Header />
    <DayCalender />
    </div>
  );
}

export default App;
