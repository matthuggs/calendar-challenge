import React from 'react';
import classes from './DayEvent.module.css';

const dayEvent = (props) => {

    const style = {
        top: props.dayEvent.start,
        height: (props.dayEvent.end - props.dayEvent.start),
        width: props.dayEvent.width + '%',
        left: props.dayEvent.left + '%',
        backgroundColor: props.dayEvent.color
    }

    return (
        <div style={style} className={classes.DayEvent}></div>
    )
}

export default dayEvent;