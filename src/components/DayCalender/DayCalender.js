import React, {Component} from 'react';
import DayEvent from '../DayEvent/DayEvent';
import classes from './DayCalender.module.css';
import {HOURS, renderDay } from '../../scripts/renderDay';

class DayCalender extends Component {

    state = {
        dayEvents:[
            {start:0, end: 60},
            {start:30, end: 90},
            {start:20, end: 80},
            {start:300, end: 330},
        ],
        dayComponents:[]
    }

    render(){

        let dayEvents = renderDay(this.state.dayEvents);

        window.renderDay = (events) => {
            this.setState({...this.state, dayEvents: events})
            dayEvents = renderDay(this.state.dayEvents);
        }

        return(
            <div className={classes.DayCalendar}>
                { HOURS.map((time, index) => <div className={classes.DaySection} key={index}>{time}</div>)}
                {dayEvents.map((result, index) => <DayEvent dayEvent={result} key={result.id}/>)}
            </div>
        )
    }
}

export default DayCalender;