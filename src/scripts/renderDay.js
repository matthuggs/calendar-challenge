/**
 * collision detection between two events. return true if two events overlap and false if not.
 * @param {*} currentEvent 
 * @param {*} nextEvent 
 */
const isCollision = (currentEvent, nextEvent) => {
    const isStartConflict = (currentEvent.start >= nextEvent.start && currentEvent.start <= nextEvent.end);
    const isEndConflict = (currentEvent.end >= nextEvent.start && currentEvent.end <= nextEvent.end);
    return (isStartConflict || isEndConflict);
}

/**
 * filter results callback.
 * @param {*} next 
 * @param {*} current 
 */
const filterResults = (next, current) => {
    return isCollision(current, next);
}

/**
 * When filtering results check if the result has previously been inserted to results array.
 * @param {*} obj 
 * @param {*} result 
 */
const isFiltered = (obj, result) => {
    return obj.id === result.id;
}

/**
 * Set the results for the grid to be rendered.
 * loop through each event, 
 * check if the current event collides with other events and produce array of all conflicting events.
 * loop through the filtered results, if the event has benn filtered don't push to the res
 * @param {*} dayEvents 
 */
export const renderDay = (dayEvents) => {
    let results = [];
    for(var i = 0; i < dayEvents.length; i ++){
        //take the first result
        var current = dayEvents[i];
        current.id = 'e_' + i;
        //check if is in the results.
        let filteredResults = dayEvents.filter(filterResults.bind(this, current));
        //do the editing
        for(let j = 0; j < filteredResults.length; j++){
            //if you exisit in results escape.
            const isInArray = results.some(isFiltered.bind(this,filteredResults[j]));
            if(isInArray) continue;
            filteredResults[j].width = Math.floor(100 / filteredResults.length);
            filteredResults[j].left = j * filteredResults[j].width;
            filteredResults[j].color = (filteredResults.length > 1)? '#cc000050' : '#00cc5350'
            results.push(filteredResults[j]); 
        } 
    }
    console.log(results);
    return results;  
}

 export const HOURS = ['09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00'];